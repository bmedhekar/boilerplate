<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Boilerplate</title>
    <meta name="description" content="">

    <!-- css group start -->
    <?php include 'view/include_css.html' ?>
    <!-- css group end -->

</head>

<body>
    <header>
        <div class="comp-header">
            <!-- header start -->
            <?php include 'view/header.html' ?>
            <!-- header end -->
        </div>
    </header>


    <main>
        <div class="main">

            <div class="comp-section step-1 active">
                <div class="container">
                    <div class="title-wrap">
                        <h2 class="title"><span class="step-count">01</span> Project Details</h2>
                    </div>
                    <div class="cont-wrap">
                        <form class="bootstrap-form card project-info-form">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="pname">Project name</label>
                                        <input type="text" class="form-control" id="pname" name="project_name">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="ptype">Project Type</label>
                                        <input type="text" class="form-control" id="ptype" name="project_type">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="pdesc">Project description</label>
                                        <textarea name="project_desc" id="pdesc" cols="30" rows="4" class="form-control"
                                            name="project_desc"></textarea>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="author">Author</label>
                                        <input type="text" class="form-control" id="author" name="author_name">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="cname">Client Name</label>
                                        <input type="text" class="form-control" id="cname" name="client">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="cname">Technology</label>
                                        <input type="text" class="form-control" id="technology" name="technology">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="btn-wrap"><button data-show="step-2" type="submit"
                                            class="btn btn-primary btn-project-info next-step-btn">Next</button></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="comp-section step-2 theme-dark">
                <div class="container">
                    <div class="title-wrap">
                        <h2 class="title"><span class="step-count">02</span> Bootstrap</h2>
                    </div>
                    <div class="cont-wrap">
                        <form class="bootstrap-form card bootstrap-info-form">
                            <h3 class="form-title" for="bootstrap">Do you widh to add bootstrap?</h3>
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="radio" id="yes-default" data-rel="bootstrap-reqd"
                                            class="js-radio-select" value="yes-default" name="bootstrap_reqd">
                                        <label for="yes-default">Yes (default)</label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="radio" id="yes-cust" value="yes-custom" data-rel="bootstrap-reqd"
                                            class="js-radio-select" name="bootstrap_reqd">
                                        <label for="yes-cust">Yes (Customize)</label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="radio" id="no" data-rel="bootstrap-reqd" class="js-radio-select"
                                            value="no-bootstrap" name="bootstrap_reqd">
                                        <label for="no">No</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row js-option-list bootstrap-reqd">
                                <div class="col-12">
                                    <h3 class="form-title" for="bootstrap">Choose from the list of components.</h3>
                                    <div class="row checkbox-group">
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[0]" value="_alert" class="checkbox" id="bs-alert">
                                                <label for="bs-alert">alert</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[1]" value="_badge" class="checkbox" id="bs-badge">
                                                <label for="bs-badge">badge</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[2]" value="_breadcrumb" class="checkbox" id="bs-breadcrumb">
                                                <label for="bs-breadcrumb">breadcrumb</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[3]" value="_button-group" class="checkbox" id="bs-button-group">
                                                <label for="bs-button-group">button group</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[4]" value="_buttons" class="checkbox" id="bs-buttons">
                                                <label for="bs-buttons">buttons</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[5]" value="_card" class="checkbox" id="bs-card">
                                                <label for="bs-card">card</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[6]" value="_carousel" class="checkbox" id="bs-carousel">
                                                <label for="bs-carousel">carousel</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[7]" value="_close" class="checkbox" id="bs-close">
                                                <label for="bs-close">close</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[8]" value="_code" class="checkbox" id="bs-code">
                                                <label for="bs-code">code</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[9]" value="_custom-forms" class="checkbox" id="bs-custom-forms">
                                                <label for="bs-custom-forms">custom forms</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[10]" value="_dropdown" class="checkbox" id="bs-dropdown">
                                                <label for="bs-dropdown">dropdown</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[11]" value="_forms" class="checkbox" id="bs-forms">
                                                <label for="bs-forms">forms</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[12]" value="_functions" class="checkbox" id="bs-functions">
                                                <label for="bs-functions">functions</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[13]" value="_grid" class="checkbox" id="bs-grid">
                                                <label for="bs-grid">grid</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[14]" value="_images" class="checkbox" id="bs-images">
                                                <label for="bs-images">images</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[15]" value="_input-group" class="checkbox" id="bs-input-group">
                                                <label for="bs-input-group">input group</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[16]" value="_jumbotron" class="checkbox" id="bs-jumbotron">
                                                <label for="bs-jumbotron">jumbotron</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[17]" value="_list-group" class="checkbox" id="bs-list-group">
                                                <label for="bs-list-group">list-group</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[18]" value="_media" class="checkbox" id="bs-media">
                                                <label for="bs-media">media</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[19]" value="_mixins" class="checkbox" id="bs-mixins">
                                                <label for="bs-mixins">mixins</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[20]" value="_modal" class="checkbox" id="bs-modal">
                                                <label for="bs-modal">modal</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[21]" value="_nav" class="checkbox" id="bs-nav">
                                                <label for="bs-nav">nav</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[22]" value="_navbar" class="checkbox" id="bs-navbar">
                                                <label for="bs-navbar">navbar</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[23]" value="_pagination" class="checkbox" id="bs-pagination">
                                                <label for="bs-pagination">pagination</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[24]" value="_popover" class="checkbox" id="bs-popover">
                                                <label for="bs-popover">popover</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[25]" value="_print" class="checkbox" id="bs-print">
                                                <label for="bs-print">print</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[26]" value="_progress" class="checkbox" id="bs-progress">
                                                <label for="bs-progress">progress</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[27]" value="_reboot" class="checkbox" id="bs-reboot">
                                                <label for="bs-reboot">reboot</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[28]" value="_root" class="checkbox" id="bs-root">
                                                <label for="bs-root">root</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[29]" value="_tables" class="checkbox" id="bs-tables">
                                                <label for="bs-tables">tables</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[30]" value="_tooltip" class="checkbox" id="bs-tooltip">
                                                <label for="bs-tooltip">tooltip</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[31]" value="_transitions" class="checkbox" id="bs-transitions">
                                                <label for="bs-transitions">transitions</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[32]" value="_type" class="checkbox" id="bs-type">
                                                <label for="bs-type">type</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[33]" value="_utilities" class="checkbox" id="bs-utilities">
                                                <label for="bs-utilities">utilities</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="checkbox" name="chk-bootstrap-scss[34]" value="_variables" class="checkbox" id="bs-variables">
                                                <label for="bs-variables">variables</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="btn-wrap"><button data-show="step-3" type="submit"
                                            class="btn btn-primary btn-bootstrap next-step-btn">Next</button></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="comp-section step-3">
                <div class="container">
                    <div class="title-wrap">
                        <h2 class="title"><span class="step-count">03</span> SCSS folder structure</h2>
                    </div>
                    <div class="cont-wrap">
                        <form class="bootstrap-form card scss-structure-form">
                            <h3 class="form-title">Do you wish to customize SCSS folder structure?</h3>
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="radio" class="js-radio-select" id="customize-scss-yes"
                                            value="yes-custom" name="scss_folder" data-rel="scss-folder">
                                        <label for="customize-scss-yes">Yes</label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="radio" class="js-radio-select" id="customize-scss-no"
                                            value="no-cust" name="scss_folder" data-rel="scss-folder">
                                        <label for="customize-scss-no">No</label>
                                    </div>
                                </div>
                                <div class="col-12 js-option-list scss-folder">
                                    <h3 class="form-title">Choose from the list of components.</h3>
                                    <div class="row checkbox-group">
                                        <div class="col-3">
                                            <div class="form-group typ-checkbox">
                                                <input type="checkbox" name="chk-scss-folder[0]" value="base" class="checkbox" id="base">
                                                <label for="base">base</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group typ-checkbox">
                                                <input type="checkbox" name="chk-scss-folder[1]" value="component" class="checkbox" id="component">
                                                <label for="component">component</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group typ-checkbox">
                                                <input type="checkbox" name="chk-scss-folder[2]" value="layout" class="checkbox" id="layout">
                                                <label for="layout">layout</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group typ-checkbox">
                                                <input type="checkbox" name="chk-scss-folder[3]" value="extended" class="checkbox" id="extended">
                                                <label for="extended">extended</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group typ-checkbox">
                                                <input type="checkbox" name="chk-scss-folder[4]" value="module" class="checkbox" id="module">
                                                <label for="module">module</label>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group typ-checkbox">
                                                <input type="checkbox" name="chk-scss-folder[5]" value="bootstrap" class="checkbox" id="bootstrap">
                                                <label for="bootstrap">bootstrap</label>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="col-12">
                                    <div class="btn-wrap"><button data-show="step-4" type="submit"
                                            class="btn btn-primary btn-scss-folder next-step-btn">Next</button></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="comp-section step-4 theme-dark">
                <div class="container">
                    <div class="title-wrap">
                        <h2 class="title"><span class="step-count">04</span> Basic Confugration</h2>
                    </div>
                    <div class="cont-wrap">
                        <form class="bootstrap-form card basic-config-form">
                            <h3 class="form-title">Primary (theme) Colors</h3>
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="primary-color">Primary Color <span class="note">( Default:
                                                #3aae97 )</span></label>
                                        <input type="text" class="form-control" id="primary-color" name="primary_color">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="secondary-color">Secondary Color <span class="note">( Default:
                                                #3aae97 )</span></label>
                                        <input type="text" class="form-control" id="secondary-color"
                                            name="secondary_color">
                                    </div>
                                </div>
                            </div>
                            <h3 class="form-title">Base (body text) Colors</h3>
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="base-dark-color">Dark Color <span class="note">( Default:
                                                #040404 )</span></label>
                                        <input type="text" class="form-control" id="base-dark-color"
                                            name="base_dark_color">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="base-light-color">Light Color <span class="note">( Default:
                                                #727781 )</span></label>
                                        <input type="text" class="form-control" id="base-light-color"
                                            name="base_light_color">
                                    </div>
                                </div>
                            </div>
                            <h3 class="form-title">Responsive media queries</h3>
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="media-1">Extra large devices </label>
                                        <input type="text" class="form-control" id="media-1" name="media_1"
                                            value="1200">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="media-2">Large devices </label>
                                        <input type="text" class="form-control" id="media-2" name="media_2" value="992">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="media-3">Medium devices </label>
                                        <input type="text" class="form-control" id="media-3" name="media_3" value="768">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="media-4">Small devices </label>
                                        <input type="text" class="form-control" id="media-4" name="media_4" value="576">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="btn-wrap"><button data-show="step-5" type="submit"
                                            class="btn btn-primary btn-basic-info next-step-btn">Next</button></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="comp-section step-5">
                <div class="container">
                    <div class="title-wrap">
                        <h2 class="title"><span class="step-count">05</span> Plugins Information</h2>
                    </div>
                    <div class="cont-wrap">
                        <form class="bootstrap-form card plugin-form">
                            <h3 class="form-title" for="bootstrap">Select required jQuery plugins</h3>
                            <div class="row">
                                <!-- <div class="col-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="chk-plugins[0]" value="tab" class="checkbox" id="tab-js">
                                        <label for="tab-js">Tab</label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="chk-plugins[1]" value="accordion" class="checkbox" id="accordion-js">
                                        <label for="accordion-js">accordion</label>
                                    </div>
                                </div> -->
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="chk-plugins[2]" value="swiper.5.2.1.min" class="checkbox" id="swiper-js" >
                                        <label for="swiper-js">Swiper slider</label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="chk-plugins[3]" value="nouislider.14.1.1.min" class="checkbox" id="noui-js">
                                        <label for="noui-js">Noui slider</label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="chk-plugins[4]" value="datepicker" class="checkbox" id="datepicker-js">
                                        <label for="datepicker-js">datepicker</label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="chk-plugins[5]" value="datatables.1.1.2.min" class="checkbox" id="datatable-js">
                                        <label for="datatable-js">datatable</label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="chk-plugins[6]" value="mCustomScrollbar.3.1.5.min" class="checkbox" id="mCustomScrollbar-js">
                                        <label for="mCustomScrollbar-js">mCustomScrollbar</label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="chk-plugins[7]" value="select2.4.0.12.min" class="checkbox" id="select2-js">
                                        <label for="select2-js">select2</label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="chk-plugins[8]" value="device.0.9.1.min" class="checkbox" id="device-js">
                                        <label for="device-js">device</label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="chk-plugins[9]" value="modernizr.3.6.0.min" class="checkbox" id="modernize-js">
                                        <label for="modernize-js">modernize</label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="chk-plugins[10]" value="chart.2.9.3.min.js" class="checkbox" id="chart-js">
                                        <label for="chart-js">chart.js</label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="chk-plugins[11]" value="highcharts.8.0.min" class="checkbox" id="highchart-js">
                                        <label for="highchart-js">highchart.js</label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="chk-plugins[12]" value="wow.1.3.0.min" class="checkbox" id="wow-js">
                                        <label for="wow-js">wow.js</label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="chk-plugins[13]" value="aos.2.3.1.min" class="checkbox" id="aos-js">
                                        <label for="aos-js">AOS,js</label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="chk-plugins[14]" value="skrollr.0.6.30.min" class="checkbox" id="skrollr-js">
                                        <label for="skrollr-js">skrollr</label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <input type="checkbox" name="chk-plugins[15]" value="gsap.3.0.2.min" class="checkbox" id="greensock-js">
                                        <label for="greensock-js">Greensock</label>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-12">
                                    <div class="btn-wrap">
                                        <button type="submit" class="btn btn-primary">Download Base Folder</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <div class="overlay"></div>
    <div class="comp-modal">
        <!-- <a href="" class="modal-close">x</a> -->
        <div class="card">
            <!-- <h1 class="title">Thank You for visiting !!</h1> -->
            <h1 class="title">Happy Coding !!</h1>
            <p class="desc">If download does not start in 5 secs, click below</p>
            <div class="btn-wrap">
                <a href="" class="btn btn-primary">Download Here</a>
            </div>
            <div class="btn-wrap">
                <a href="#" class="btn btn-link btn-reset">RESET</a> or
                <a href="#" class="btn btn-link modal-close">EDIT</a>
            </div>
        </div>
    </div>

    <footer>
        <!-- header start -->
        <?php include 'view/footer.html' ?>
        <!-- header end -->
    </footer>

    <!-- js group start -->
    <?php include 'view/include_js.html' ?>
    <!-- js group end -->

</body>

</html>