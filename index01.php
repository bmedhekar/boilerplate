<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FI - HTML Boilerplate</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <div class="container p-3">
        <div class="card p-5">
            <h2 class="pb-2">Enter your Project Name</h2>
            <form method="POST" action="action.php">
                <div class="form-group">
                    <input type="text" id="pname" name="project-name" placeholder="Enter Project name" class="form-control">
                </div>
                <input type="submit" class="btn btn-primary" value="Download">
            </form>
            <h5><?php echo $successMsg; ?></h5>
        </div>
    </div>
</body>
</html>