<?php 
    /* header for zip file */
    

    /* storing initial information */
    $data= $_POST;

    $project_name = $data['project_name'];
    $project_type = $data["project_type"];
    $project_desc = $data["project_desc"];
    $author_name=$data["author_name"];
    $client=$data["client"];
    $technology=$data["technology"];
    date_default_timezone_set('Asia/India');
    $timestamp = date('mdYhis', time());


    $baseurl =__DIR__ . "/base-template-".$project_name;

    // if(isset($_POST["pname"])){

        // create folders
        mkdir($baseurl, 0777); 
        mkdir($baseurl."/assets", 0777);

        mkdir($baseurl."/assets/js", 0777);
        mkdir($baseurl."/assets/js/libraries", 0777);
        
        mkdir($baseurl."/assets/css", 0777);
        mkdir($baseurl."/assets/css/libraries", 0777);
        
        mkdir($baseurl."/assets/scss", 0777);        
        
        mkdir($baseurl."/assets/images", 0777);
        mkdir($baseurl."/assets/fonts", 0777);
        mkdir($baseurl."/assets/json", 0777);
        mkdir($baseurl."/assets/videos", 0777);

        //creating files
        $index_content='<!DOCTYPE html>
        <html lang="en">
        
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
            <title>'.$project_name.'</title>
            <style type="text/css">
                body {
                    font-family: "Roboto", sans-serif;
                    margin: 0;
        
                }
        
                .logo {
                    margin-left: 30px;
                    margin-top: 30px;
                    padding: 8px;
                }
        
                ul,
                li {
                    margin: 0;
                    padding: 0;
                }
        
                a {
                    text-decoration: none;
                }
        
        
                .siteContainer {
                    font-size: 18px;
                    line-height: 1.3rem;
                    margin-left: 50px;
                    margin-top: 40px;
                }
        
                .siteContainer ul li {
                    color: #262262;
                    font-size: 90%;
                    font-weight: normal;
                    list-style: none outside none;
                    margin-left: -8px;
                    min-height: 24px;
                    position: relative;
                    padding-left: 5px;
                }
        
                .siteContainer>ul>li {
                    padding: 10px 0 10px;
                }
        
                .siteContainer>ul>li:nth-child(odd):after {
                    content: "";
                    left: -50px;
                    z-index: -2;
                    height: 100%;
                    position: absolute;
                    top: 0;
                    background: #f2f2f2;
                    right: 0;
                }
        
                .siteContainer ul li:before {
                    content: "";
                    border: 8px solid transparent;
                    border-left: 8px solid #888;
                    display: inline-block;
                    position: relative;
                    position: absolute;
                    left: 6px;
                    top: 18px;
                    z-index: -1;
                    /* transform: rotate(45deg);
                    transform-origin: left; */
                    transition: all 0.3s ease;
                }
        
                .siteContainer ul li.open:before {
                    transform: rotate(45deg);
                    transform-origin: left;
                }
        
                .siteContainer ul li .subMenu li:before {
                    transform: rotate(0);
                    transform-origin: left;
                    border: 6px solid transparent;
                    border-left: 8px solid #888;
                    top: 11px;
                }
        
                .siteContainer ul li.parent:before {
                    border-left: 8px solid #000;
                }
        
                .siteContainer ul li .subMenu {
                    padding-left: 34px;
                    position: relative;
                }
        
                .siteContainer ul li .subMenu li {
                    padding-left: 30px;
                }
        
                .siteContainer ul li .lbl {
                    padding-left: 30px;
                }
        
                .siteContainer ul li a {
                    color: #262262;
                    display: inline-block;
                    text-transform: capitalize;
                    padding: 6px 0 7px;
                }
                
                .siteContainer ul li a:hover,
                .siteContainer h4 a:hover {
                    color: #f7941e;
                }
            </style>
        </head>
        
        <body>
            <img class="logo" src="assets/images/logo.svg" alt="logo" height="30px">
            <div class="siteContainer">
                <ul class="custAcc">
                    <li>
                        <div class="lbl">
                            <a href="template.shtml">Template</a>
                        </div>
                    </li>
                    <li>
                        <div class="lbl">
                            <a href="components.shtml">Component Sheet</a>
                        </div>
                    </li>
                    <!-- <li class="parent open">
                        <div class="lbl">
                            <a href="components.shtml">Component Sheet</a>
                        </div>
                        <ul class="subMenu">
                            <li>
                                <a href="#">sublink 1 </a>
                            </li>
                            <li>
                                <a href="#">sublink 2</a>
                            </li>
                        </ul>
                    </li>-->
                    
                </ul>
            </div>
            <script src="assets/js/libraries/jquery.js"></script>
            </script>
            <script type="text/javascript">
                $(function () {
                    $(".lbl").click(function () {
                        //console.log($(this).parent().find(".subMenu").length);
                        if ($(this).parent().find(".subMenu").length) {
                            if ($(this).parent().hasClass("open")) {
                                $(this).parent().removeClass("open");
                                $(this).parent().find("> .subMenu").stop(true, true).slideUp()
                            } else {
                                $(this).parent().addClass("open");
                                $(this).parent().find("> .subMenu").stop(true, true).slideDown()
                            }
                        }
                    })
        
                })
            </script>
        </body>
        
        </html>';


        $fp = fopen($baseurl."/index.html","wb");
        fwrite($fp,$index_content);
        fclose($fp);

        /* copy files from repositary to build */
        copy(__DIR__ . '/repositary/.htaccess',$baseurl . '/.htaccess');
        copy(__DIR__ . '/repositary/.gitignore',$baseurl . '/.gitignore');
        copy(__DIR__ . '/repositary/package.json',$baseurl . '/package.json');
        copy(__DIR__ . '/repositary/template.shtml',$baseurl . '/template.shtml');
        copy(__DIR__ . '/repositary/js/jquery-3.1.1.min.js',$baseurl . '/assets/js/libraries/jquery-3.1.1.min.js');
        copy(__DIR__ . '/repositary/js/custom.js',$baseurl . '/assets/js/custom.js');


        

        /* Scss folder section condition */
        if($data["scss_folder"]=="yes-custom"){
            foreach ($data['chk-scss-folder'] as $ScssFolder){
                mkdir($baseurl."/assets/scss/".$ScssFolder, 0777);
            }

        }else{
            mkdir($baseurl."/assets/scss/base", 0777);
            mkdir($baseurl."/assets/scss/layout", 0777);
            mkdir($baseurl."/assets/scss/component", 0777);
            mkdir($baseurl."/assets/scss/module", 0777);
            mkdir($baseurl."/assets/scss/extended", 0777);
            mkdir($baseurl."/assets/scss/bootstrap", 0777);
        }

        /* bootstrap section conditions */
        $bootsrapForCustomScss="/* Bootstrap */";
        if($data["bootstrap_reqd"]=="yes-default"){
            copy(__DIR__ . '/repositary/bootstrap/js/bootstrap.min.js',$baseurl . '/assets/js/libraries/bootstrap.min.js');
            copy(__DIR__ . '/repositary/bootstrap/css/bootstrap-reboot.min.css',$baseurl . '/assets/css/libraries/bootstrap-reboot.min.css');
            copy(__DIR__ . '/repositary/bootstrap/css/bootstrap.min.css',$baseurl . '/assets/css/libraries/bootstrap.min.css');
            copy(__DIR__ . '/repositary/bootstrap/css/bootstrap-grid.min.css',$baseurl . '/assets/css/libraries/bootstrap-grid.min.css');

        }else if($data["bootstrap_reqd"]=="yes-custom"){
            if(!is_dir($baseurl."/assets/scss/bootstrap")) {
                mkdir($baseurl."/assets/scss/bootstrap", 0777);
            }
            copy(__DIR__ . '/repositary/bootstrap/scss/_root.scss',$baseurl . '/assets/scss/base/_root.scss');
            foreach ($data['chk-bootstrap-scss'] as $BSfile){
                copy(__DIR__ . '/repositary/bootstrap/scss/'.$BSfile.'.scss',$baseurl . '/assets/scss/bootstrap/'.$BSfile.'.scss');
                $bootsrapForCustomScss.="\n".'@import "bootstrap/'.$BSfile.'";';
            }
        }else{
            copy(__DIR__ . '/repositary/bootstrap/scss/_root.scss',$baseurl . '/assets/scss/base/_root.scss');
        }
        

        /* Basic configration setup */
        copy(__DIR__ . '/repositary/scss/config/_mixin.scss',$baseurl . '/assets/scss/base/_mixin.scss');

        $variables_content='/* colors */
                
        $clr-white:#ffffff;
        $clr-black:#000000;
        $clr-primary:'.$data['primary_color'].';
        $clr-secondary:'.$data['secondary_color'].';
        $clr-base-dark:'.$data['base_dark_color'].';
        $clr-base-light:'.$data['base_light_color'].';
        
        /*font family*/
        
        $font-regular:400;
        $font-light:300;
        $font-medium:500;
        $font-semibold:600;
        $font-bold:700;
        
        /* breakpoints */
        
        $media-xs:'.$data['media_1'].'px;
        $media-md:'.$data['media_2'].'px;
        $media-lg:'.$data['media_3'].'px;
        $media-xl:'.$data['media_4'].'px;';

        $fpVar = fopen($baseurl."/assets/scss/base/_variables.scss","wb");
        fwrite($fpVar,$variables_content);
        fclose($fpVar);


        /* plugins addition */
        $js_for_grunt="";
        $includeCss="";
        $includeJs='"<script src="assets/js/libraries/jquery-3.1.1.min.js"></script>';
        foreach ($data['chk-plugins'] as $plugin){
            $pluginPath = glob(__DIR__ . '/repositary/js/'.$plugin.'*');
            $pluginPath = pathinfo($pluginPath[0]);
            $pluginName = $pluginPath['filename'];
            copy(__DIR__ . '/repositary/js/'.$pluginName.'.js',$baseurl . '/assets/js/libraries/'.$pluginName.'.js');

            if($plugin=="wow.1.3.0.min"){
                copy(__DIR__ . '/repositary/css/animate.css',$baseurl . '/assets/css/libraries/animate.css');

            }else{
                copy(__DIR__ . '/repositary/css/'.$pluginName.'.css',$baseurl . '/assets/css/libraries/'.$pluginName.'.css');
            }

            $js_for_grunt.=",'assets/js/libraries/".$pluginName.".js'";
            $includeCss=$includeCss.'<link rel="stylesheet" type="text/css" href="assets/css/libraries/'.$pluginName.'.css">';
            $includeJs=$includeJs.'<script src="assets/js/libraries/'.$pluginName.'.js"></script>';
        }

        //echo "gruntvar=".$js_for_grunt;
        /* grunt file creation */
        $grunt_content= "module.exports = function(grunt) {
            grunt.initConfig({
                pkg: grunt.file.readJSON('package.json'),
        
                //task for watching changes in scss & js
                watch: {
                    grunt: { files: ['Gruntfile.js'] },
                    scss: {
                        files: ['assets/scss/**/*.scss'],
                        tasks: ['scss', 'cssmin']
                    },
                    // js: {
                    //     files: ['assets/js/**/*.js'],
                    //     tasks: ['uglify']
                    // }
                },
        
                //browser sync task for auto refresh changes on browser
                browserSync: {
                    dev: {
                        bsFiles: {
                            src: [
                                'assets/css/*.css',
                                './**/*.shtml',
                                './view/**/*.html'
                            ]
                        },
                        options: {
                            watchTask: true,
                            //updated your url here before starting work
                            proxy: 'http://all.loc/payu/git/'
                        }
                    }
                },
        
                // scss watch task
                scss: {
                    dist: {
                        options: {
                            style: 'compressed',
                            precision: 2
                        },
                        files: {
                            'assets/css/custom.css': 'assets/scss/custom.scss',
                        }
                    }
                },
        
                //uglify js (update list of js used in project here)
                uglify: {
                    options: {
                        mangle: false,
                        beautify: true,
                        compress: false
                    },
                    my_target: {
                        files: {
                            'assets/js/vendor.js': [
                                'assets/js/libraries/jquery-3.1.1.min.js'"
                                .$js_for_grunt.
                                
                            "],
                            'assets/js/custom.js': [
                                'assets/js/custom.js'
                            ]
                        }
                    }
                },
        
                //clean previous build before creating a new
                clean: {
                    build: {
                        src: ['build/**/*.*']
                    }
                },
        
                //copy htmls & assets to build folder from production
                copy: {
                    html: {
                        expand: true,
                        cwd: '',
                        src: ['*.html', 'view/*.html'],
                        dest: 'build/',
        
                    },
                    shtml: {
                        files: [{
                            expand: true,
                            dot: true,
                            src: ['*.shtml'],
                            dest: 'build/',
                            rename: function(dest, src) {
                                return dest + src.replace(/\.shtml$/, '.html');
                            }
                        }]
                    },
                    images: {
                        expand: true,
                        cwd: 'assets/images/',
                        src: '**',
                        dest: 'build/assets/images/'
                    },
                    fonts: {
                        expand: true,
                        cwd: 'assets/fonts/',
                        src: '**',
                        dest: 'build/assets/fonts/'
        
                    },
                    css: {
                        expand: true,
                        cwd: 'assets/css',
                        src: '*.css',
                        dest: 'build/assets/css/'
        
                    },
                    video: {
                        expand: true,
                        cwd: 'assets/videos',
                        src: '*.*',
                        dest: 'build/assets/videos/'
        
                    },
                    js: {
                        expand: true,
                        cwd: 'assets/js',
                        src: '*.js',
                        dest: 'build/assets/js/'
                    }
                },
        
                //replce shtml includes with actual htmls
                ssi: {
                    options: {
                        cache: 'all',
                        ext: '.shtml',
                        baseDir: 'build',
                    },
                    target: {
                        files: [{
                            expand: true,
                            cwd: 'build',
                            src: ['*.html'],
                            dest: 'build'
                        }]
                    }
                },
        
                //minify css
                cssmin: {
                    target: {
                        files: {
                            'assets/css/libraries.css': ['assets/css/libraries/*.css'],
                            'assets/css/custom.css': ['assets/css/custom.css'],
                        }
                    }
                },
        
                //minified images
                tinypng: {
                    options: {
                        apiKey: 'ognQnVUe4Opz6Q4sMmAVvYZ1hJWh3UYK',
                        summarize: true
                    },
                    compress: {
                        expand: true,
                        src: 'build/assets/images/*.png',
                        dest: './',
                        ext: '.png'
                    },
                    compress2: {
                        expand: true,
                        src: 'build/assets/images/*.jpg',
                        dest: './',
                        ext: '.jpg'
                    },
                },
        
                //replace css & js grouping occurrence 
                usemin: {
                    html: 'build/*.html'
                },
        
                //replace all text occurrence '.shtml' with '.html' 
                replace: {
                    files: {
                        src: 'build/*.html', // source files array (supports minimatch)
                        dest: 'build/', // destination directory or file
                        force: true,
                        replacements: [{
                            from: '.shtml', // string replacement
                            to: '.html'
                        }]
                    }
                }
            });
        
        
        
            //task registrations 
            grunt.loadNpmTasks('grunt-ssi');
            grunt.loadNpmTasks('grunt-contrib-scss');
            grunt.loadNpmTasks('grunt-contrib-cssmin');
            grunt.loadNpmTasks('grunt-usemin');
            grunt.loadNpmTasks('grunt-contrib-copy');
            grunt.loadNpmTasks('grunt-contrib-uglify');
            grunt.loadNpmTasks('grunt-contrib-watch');
            grunt.loadNpmTasks('grunt-browser-sync');
            grunt.loadNpmTasks('grunt-tinypng');
            grunt.loadNpmTasks('grunt-contrib-clean');
            grunt.loadNpmTasks('grunt-text-replace');
        
            //default task watch & browsersync
            grunt.registerTask('default', ['browserSync', 'watch']);
        
            //grunt build task(copy,ssi,cssmin,usemin,replace)
            grunt.registerTask('build', ['clean', 'copy', 'ssi', 'cssmin', 'usemin', 'replace']);
        
        }";

        $fpGrunt = fopen($baseurl."/Gruntfile.js","wb");
        fwrite($fpGrunt,$grunt_content);
        fclose($fpGrunt);




        /* include files creation */
        mkdir($baseurl."/view", 0777);
        copy(__DIR__ . '/repositary/view/header.html',$baseurl . '/view/header.html');
        copy(__DIR__ . '/repositary/view/footer.html',$baseurl . '/view/footer.html');
        copy(__DIR__ . '/repositary/view/popup.html',$baseurl . '/view/popup.html');

        //echo $includeCss;


        $includeCss_content='<!-- build:css assets/css/libraries.css -->'
        .$includeCss.
        '<!-- endbuild -->
        <link rel="stylesheet" type="text/css" href="assets/css/icon.css">
        <link rel="stylesheet" type="text/css" href="assets/css/custom.css">';

        $fpInCss = fopen($baseurl."/view/include_css.html","wb");
        fwrite($fpInCss,$includeCss_content);
        fclose($fpInCss);

        $includeJs_content='<!-- build:js assets/js/vendor.js -->'
        .$includeJs.
        '<!-- endbuild -->
        
        <script src="assets/js/custom.js"></script>';

        $fpInJs = fopen($baseurl."/view/include_js.html","wb");
        fwrite($fpInJs,$includeJs_content);
        fclose($fpInJs);

        
        /* creating custom.scss */
        copy(__DIR__ . '/repositary/scss/base/base.scss',$baseurl . '/assets/scss/base/base.scss');

        $customScss_content='/*
        @Project : '.$project_name.'
        @Desc : '.$project_desc.'
        @type : '.$project_type.'
        @Author : '.$author_name.'
        @Date : '.$timestamp.';
      */
    
    
        /* config */
        
        // @import "base/fonts";
        @import "config/mixin";
        @import "config/variables";
        
        /* base include */
        
        @import "base/_reboot";
        
        /* extented */
        
        // @import "extended/example";
        
        /* layout */
        
        //@import "layout/lyt-example";

        /* component */
        
        //@import "component/comp-example";'."\n"

        .$bootsrapForCustomScss;
    
        $fpCustomScss = fopen($baseurl."/assets/scss/custom.scss","wb");
        fwrite($fpCustomScss,$customScss_content);
        fclose($fpCustomScss);

    // }


    

    Zip($baseurl, $baseurl.'.zip');

    //header for downloading zip
    $zippath = $baseurl.'.zip';
    // mkdir($zippath.'/', 0777);

    // echo $zippath;
    ob_start();
    
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-type: application/octet-stream");
    header("Content-Disposition: attachment; filename=\"".basename($zippath)."\"");
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: ".filesize($zippath));
    
    readfile($zippath); 
    
    // set_time_limit(0);
    // $file = @fopen($zippath,"rb");
    // while(!feof($file))
    // {
    //     print(@fread($file, 1024*8));
    //     ob_flush();
    //     flush();
    // }

    
    // ob_clean();
    // flush();
    // exit;
    

    //call to delete_files function
    //delete_files($baseurl . '/');
    // $successMsg = "Your inital setup for".$project_name." is ready !! Enjoy !!";                
    // echo "success";

    // print_r($data);



    /* function to create zip folder & delete the same folder */
    function Zip($source, $destination){
        if (!extension_loaded('zip') || !file_exists($source)) {
            return false;
        }

        $zip = new ZipArchive();
        if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
            return false;
        }

        $source = str_replace('\\', '/', realpath($source));

        if (is_dir($source) === true)
        {
            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

            foreach ($files as $file)
            {
                $file = str_replace('\\', '/', $file);

                // Ignore "." and ".." folders
                if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                    continue;

                $file = realpath($file);

                if (is_dir($file) === true)
                {
                    $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                }
                else if (is_file($file) === true)
                {
                    $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                }
            }
        }
        else if (is_file($source) === true)
        {
            $zip->addFromString(basename($source), file_get_contents($source));
        }

        return $zip->close();        

    }


    /* funciton to delete folder with files */
    function delete_files($target) {
        if(is_dir($target)){
            $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned
    
            foreach( $files as $file ){
                delete_files( $file );      
            }
    
            rmdir( $target );
        } elseif(is_file($target)) {
            unlink( $target );  
        }
    }
?>