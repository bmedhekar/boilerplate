<?php
    $data= $_POST;
    // echo (count($data['chk-folder']));
    $i=1;
    foreach ($data['chk-folder'] as $key => $folder){
        delete_files('downloads/'.$folder);
        if($i == count($data['chk-folder'])) {
            echo json_encode("done");
            break;
        }
        $i++;
    }
    

    /* funciton to delete folder with files */
    function delete_files($target) {
        if(is_dir($target)){
            $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

            foreach( $files as $file ){
                delete_files( $file );      
            }

            rmdir( $target );
        } elseif(is_file($target)) {
            unlink( $target );  
        }
    }
?>