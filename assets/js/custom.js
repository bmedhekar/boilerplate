var FIBoilerplate = {};
var AllInfo, ProjectInfo, BootstrapInfo, ScssInfo, BasicInfo, PluginInfo;

var winWidth = $(window).width();

$(function() {
    FIBoilerplate.SubmitProjectInfo();
    FIBoilerplate.hideShowOptions();
    FIBoilerplate.submitBootstrapInfo();
    FIBoilerplate.SubmitScssInfo();
    FIBoilerplate.SubmitBasicInfo();
    FIBoilerplate.SubmitPluginInfo();
    FIBoilerplate.editStep();
    FIBoilerplate.modal();
    if ($('.delete-folder-form').length) {
        FIBoilerplate.DeleteUnused();
    }
    if ($('.login-form').length) {
        FIBoilerplate.loginSubmit();
    }
});

FIBoilerplate.SubmitProjectInfo = function() {
    var FormOne = $(".project-info-form").validate({
        rules: {
            project_name: {
                minlength: 2,
                required: !0
            },
            project_type: {
                minlength: 2,
                required: !0
            },
            project_desc: {
                minlength: 6,
                required: !0
            },
            author_name: {
                minlength: 2,
                required: !0
            },
            client: {
                minlength: 2,
                required: !0
            }
        },
        messages: {
            Mobile_Number: {
                minlength: "Your mobile number should be 10 digits only",
                maxlength: "Your mobile number should be 10 digits only"
            }
        },
        onfocusout: function(element) {
            $(element).valid();
        },
        errorElement: "div",
        errorClass: "error-msg",
        highlight: function(element) {
            $(element).parents(".form-group").addClass("error");
        },
        unhighlight: function(element) {
            $(element).parents(".form-group").removeClass("error");
        },
        submitHandler: function(form) {
            ProjectInfo = $(".project-info-form").serializeArray();
            // FIBoilerplate.downloadBuild();           
            $(".step-1").addClass("complete").removeClass("active");
            $(".step-2").addClass("active");
            return false;
        }
    });
};

FIBoilerplate.downloadBuild = function() {
    AllInfo = $.merge(ProjectInfo, BootstrapInfo);
    AllInfo = $.merge(AllInfo, ScssInfo);
    AllInfo = $.merge(AllInfo, BasicInfo);
    AllInfo = $.merge(AllInfo, PluginInfo);
    return $.ajax({
        type: "post",
        url: "action.php",
        data: AllInfo,
        dataType: "JSON",
        success: function(data) {
            // alert(data);
            console.log(data);
            $('.comp-modal .btn.btn-primary').attr('href', data);
            $('.overlay, .comp-modal').addClass('active');
            setTimeout(function() {
                window.location = data;
            }, 500)
        },
        failure: function(errMsg) {
            console.log(errMsg);

        }
    });
};

FIBoilerplate.submitBootstrapInfo = function() {
    var bootstrapInfo = $(".bootstrap-info-form").validate({
        rules: {
            bootstrap_reqd: {
                required: true
            },
            bootstrap_choice: {
                required: true
            }
        },
        errorElement: "div",
        errorClass: "error-msg",
        errorPlacement: function(error, element) {
            if (element.is(":radio")) {
                error.appendTo(element.closest('.bootstrap-form').find(".form-title"));
            } else { // This is the default behavior 
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
            // console.log("success");
            $('.comp-section.step-5').addClass('complete');
            BootstrapInfo = $(".bootstrap-info-form").serializeArray();
            // FIBoilerplate.downloadBuild();
            $(".step-2").addClass("complete").removeClass("active");
            $(".step-3").addClass("active");
            return false;
        }
    })
}

FIBoilerplate.SubmitScssInfo = function() {
    var bootstrapInfo = $(".scss-structure-form").validate({
        rules: {
            scss_folder: {
                required: true
            }
        },
        errorElement: "div",
        errorClass: "error-msg",
        errorPlacement: function(error, element) {
            if (element.is(":radio")) {
                error.appendTo(element.closest('.bootstrap-form').find(".form-title"));
            } else { // This is the default behavior 
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
            ScssInfo = $('.scss-structure-form').serializeArray();
            $(".step-3").addClass("complete").removeClass("active");
            $(".step-4").addClass("active");
            return false;
        }
    })
}

FIBoilerplate.SubmitBasicInfo = function() {
    $(".basic-config-form").submit(function() {
        BasicInfo = $(".basic-config-form").serializeArray();
        $(".step-4").addClass("complete").removeClass("active");
        $(".step-5").addClass("active");
        return false
    });
}

FIBoilerplate.editStep = function() {
    $(".title-wrap").click(function() {
        var closestParent = $(this).closest(".comp-section");
        if (closestParent.hasClass("complete")) {
            $(".comp-section").removeClass("active");
            closestParent.addClass("active");
        }
    });
}

FIBoilerplate.hideShowOptions = function() {
    $(".js-checkbox-select").click(function() {
        var showOptions = $(this).attr("data-rel");
        if ($(".js-checkbox-select").is(':checked')) {
            $("." + showOptions).slideDown();
        } else {
            $("." + showOptions).slideUp();
        }
    });

    $(".js-radio-select").click(function() {

        var showOptions = $(this).attr("data-rel");
        var chossedOption = $(this).val();
        if (chossedOption == "yes-custom") {
            $("." + showOptions).slideDown();
            $("." + showOptions + " #default").prop('checked', true);
        } else {
            $("." + showOptions).slideUp();
            $("." + showOptions + " .checkbox").prop('checked', false);
        }


        // bootstrapChkbox.change(function () {

        //     var currChkbox = $(this).attr("id");
        //     if (currChkbox == "default") {
        //         if ($(".bootstrap-reqd #default").is(":checked")) {
        //             $(".bootstrap-reqd .checkbox").prop('checked', false);
        //             console.log(23);
        //             $(".bootstrap-reqd #default").prop('checked', true);
        //             bootstrapCheckedChkboxes = 1;
        //         }
        //     }
        //     else {
        //         $(".bootstrap-reqd #default").prop('checked', false);
        //         if (bootstrapCheckedChkboxes == 0) {
        //             $(".bootstrap-reqd #default").prop('checked', true);
        //         }
        //     }
        //     bootstrapCheckedChkboxes = bootstrapChkbox.filter(':checked').length;

        //     console.log(bootstrapCheckedChkboxes);

        //     // var bootstrapCheckedChkboxes = bootstrapChkbox.filter(':checked').length;


        //     // if (bootstrapCheckedChkboxes > 1) {
        //     //     $(".bootstrap-reqd #default").prop('checked', false);
        //     // }
        //     // if (bootstrapCheckedChkboxes < 1) {
        //     //     $(".bootstrap-reqd #default").prop('checked', true);
        //     // }
        // });
    });
}

FIBoilerplate.SubmitPluginInfo = function() {
    $('.plugin-form').on('submit', function() {
        PluginInfo = $('.plugin-form').serializeArray();
        FIBoilerplate.downloadBuild();
        return false;
    });
}

FIBoilerplate.modal = function() {
    $('.comp-modal .modal-close, .overlay').on('click', function(e) {
        e.preventDefault();
        $('.comp-modal,.overlay').removeClass('active');
    });

    $('.btn-reset').on('click', function(e) {
        e.preventDefault();
        $('.comp-modal,.overlay').removeClass('active');
        $('form').trigger("reset");
    });
}

FIBoilerplate.DeleteUnused = function() {
    $('.delete-folder-form').on('submit', function(e) {
        e.preventDefault();
        console.log('submitted');
        // return false;
        var deleteList = $('.delete-folder-form').serializeArray();
        console.log(deleteList);

        $.ajax({
            type: "post",
            url: "admin-action.php",
            data: deleteList,
            dataType: "JSON",
            success: function(data) {
                // alert(data);
                console.log('sucess');
                // $('.comp-modal .btn.btn-primary').attr('href', data);
                $('.overlay, .comp-modal').addClass('active');
            },
            failure: function(errMsg) {
                console.log(errMsg);

            }
        });
    });
}


FIBoilerplate.loginSubmit = function() {
    var FormOne = $(".login-form").validate({
        rules: {
            username: {
                minlength: 2,
                required: !0
            },
            password: {
                minlength: 2,
                required: !0
            }
        },
        onfocusout: function(element) {
            $(element).valid();
        },
        errorElement: "div",
        errorClass: "error-msg",
        highlight: function(element) {
            $(element).parents(".form-group").addClass("error");
        },
        unhighlight: function(element) {
            $(element).parents(".form-group").removeClass("error");
        },
        submitHandler: function(form) {
            var LoginInfo = $(".login-form").serializeArray();
            $.ajax({
                type: "post",
                url: "login-action.php",
                data: LoginInfo,
                dataType: "JSON",
                success: function(data) {
                    // alert(data);
                    console.log(data);
                    if (data) {
                        window.location = "admin.php";
                    } else {
                        $('.invalid-cred').addClass('active');
                    }
                },
                failure: function(errMsg) {
                    console.log(errMsg);

                }
            });
            return false;
        }
    });
};