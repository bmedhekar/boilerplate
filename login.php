<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Boilerplate</title>
    <meta name="description" content="">

    <!-- css group start -->
    <?php include 'view/include_css.html' ?>
    <!-- css group end -->

</head>

<body>
    <header>
        <div class="comp-header">
            <!-- header start -->
            <?php include 'view/header.html' ?>
            <!-- header end -->
        </div>
    </header>


    <main>
        <div class="main">

            <div class="comp-section step-1 active">
                <div class="container">
                    <div class="title-wrap">
                        <h2 class="title"><span class="step-count">!!</span>Login</h2>
                    </div>
                    <div class="cont-wrap">
                        <form class="bootstrap-form card login-form">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" class="form-control" id="username" name="username">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" id="password" name="password">
                                    </div>
                                </div>                            
                                <div class="col-12">
                                    <span class="error-msg invalid-cred">Invalid Credentails !!</span>
                                    <div class="btn-wrap">
                                        <button type="submit" class="btn btn-primary btn-login">LOGIN</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
        </div>
    </main>



    <footer>
        <!-- header start -->
        <?php include 'view/footer.html' ?>
        <!-- header end -->
    </footer>

    <!-- js group start -->
    <?php include 'view/include_js.html' ?>
    <!-- js group end -->

</body>

</html>