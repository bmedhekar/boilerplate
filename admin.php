<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Boilerplate</title>
    <meta name="description" content="">

    <!-- css group start -->
    <?php include 'view/include_css.html' ?>
    <!-- css group end -->

</head>

<body>
    <header>
        <div class="comp-header">
            <!-- header start -->
            <?php include 'view/header.html' ?>
            <!-- header end -->
        </div>
    </header>


    <main>
        <div class="main">

            <div class="comp-section step-5 active">
                <div class="container">
                    <div class="title-wrap">
                        <h2 class="title"><span class="step-count">01</span> Folder listing</h2>
                    </div>
                    <div class="cont-wrap">
                        <form class="bootstrap-form card delete-folder-form">
                            <h3 class="form-title" for="bootstrap">Select files/folders to delete</h3>
                            <?php 
                                $folders = preg_grep('/^([^.])/', scandir('downloads')); 
                            ?>
                            <div class="row">
                                <?php foreach ($folders as $key => $folder) { 
                                        if(is_dir('downloads/'.$folder)){    
                                ?>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <input type="checkbox" name="chk-folder[<?php echo $key; ?>]" value="<?php echo $folder; ?>" class="checkbox" id="<?php echo $folder; ?>" >
                                            <label for="<?php echo $folder; ?>"><?php echo $folder ?></label>
                                        </div>
                                    </div>   
                                <?php }
                                } ?>                             
                            </div>


                            <div class="row">
                                <div class="col-12">
                                    <div class="btn-wrap">
                                        <button type="submit" class="btn btn-primary">Delete Now</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <div class="overlay"></div>
    <div class="comp-modal">
        <!-- <a href="" class="modal-close">x</a> -->
        <div class="card">
            <!-- <h1 class="title">Thank You for visiting !!</h1> -->
            <h1 class="title">Space restore!!</h1>
            <p class="desc">It's not just you, who need more space.</p>            
            <div class="btn-wrap">
                <a href="" class="btn btn-primary">Done</a> 
            </div>
        </div>
    </div>


    <footer>
        <!-- header start -->
        <?php include 'view/footer.html' ?>
        <!-- header end -->
    </footer>

    <!-- js group start -->
    <?php include 'view/include_js.html' ?>
    <!-- js group end -->

</body>

</html>